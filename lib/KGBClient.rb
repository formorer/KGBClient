require 'KGBClient/version'
require 'jsonrpc-client'
require 'faraday/detailed_logger'
require 'digest/sha1'
require 'json'
require 'pp'


class KGBClient
    attr_accessor :endpoint, :api_key
    
    def initialize(endpoint = nil, api_key = nil)
      @endpoint = endpoint
      @api_key = api_key
    end

    def config
      yield self
    end

    def relay_message(message:, project:, token:)


      message = {
        'version' => '1.1',
        'id' => 1,
        'method' => 'relay_message', 
        'params' => [ message, {} ] 
      }
      json = JSON.generate(message)
      sha1 = Digest::SHA1.new
      sha1 << token
      sha1 << project
      sha1 << json
      connection = Faraday.new(:url => endpoint) do |faraday|
        faraday.response :detailed_logger # <-- Inserts the logger into the connection.
        faraday.adapter  Faraday.default_adapter
        faraday.headers['X-KGB-Project'] = project
        faraday.headers['X-KGB-Auth'] = sha1.hexdigest
        faraday.headers['Content-Type'] =  'application/json'
      end
      connection.post do |req|
        req.body = json
      end
      #client = JSONRPC::Client.new(endpoint, { connection: connection })
      #client.relay_message("blah")
    end
end
